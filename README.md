### ERROR
1. GenericDataBuilder is not serializable [SaveFromGeneric](flink-parquet/src/main/java/org/example/sink/SaveFromGeneric.java)

### Parquet
1. POJO -> parquet though Avro GenericRecord, check code [SaveFromGeneric](flink-parquet/src/main/java/org/example/sink/SaveFromGeneric.java)
2. POJO -> parquet though Avro SpecificRecord, check code [SaveFromSpecific](flink-parquet/src/main/java/org/example/sink/SaveFromSpecific.java)
3. POJO -> parquet though Avro ReflectRecord, check code [SaveFromReflect](flink-parquet/src/main/java/org/example/sink/SaveFromReflect.java)
4. Read local parquet files as flink source(Reflect Type), check code [ReadFromLocalAndReflectType](flink-parquet/src/main/java/org/example/source/ReadFromReflect.java)
5. Read S3 parquet files as flink source(Specific Type), check code [ReadFromS3AndSpecificType](flink-parquet/src/main/java/org/example/source/ReadFromSpecific.java)

### S3-connector
1. flink read file from local, check code [ReadLocal.java](./s3-connector/src/main/java/org/example/connector/source/ReadLocal.java)
2. flink read file from s3, check code [ReadS3.java](./s3-connector/src/main/java/org/example/connector/source/ReadS3.java)
3. flink save file to local, check code [SaveLocal.java](./s3-connector/src/main/java/org/example/connector/sink/SaveLocal.java)
4. flink save file to s3, check code [SaveS3.java](./s3-connector/src/main/java/org/example/connector/sink/SaveS3.java)


### Clickhouse-connector
> before running you need start a clickhouse container, and execute table create command. check [init.sh](./clickhouse-connector/init.sh)
1. flink insert record in clickhouse, check code [Insert Record](./clickhouse-connector/src/main/java/org/example/connector/Insert.java)
2. flink delete record in clickhouse, check code [Delete Record](./clickhouse-connector/src/main/java/org/example/connector/Delete.java)
3. clickhouse query parquet file dir, check sql [Query](./clickhouse-connector/Query.sql)

### Arrow
1. arrow write local file, check code [Write File](./arrow-java/src/main/java/org/example/write/WriteFile.java)
2. arrow read local file, check code [Read File](./arrow-java/src/main/java/org/example/read/ReadFromFile.java)
3. arrow rpc service & client, check code [RPC example](./arrow-flight/src/main/java/org/example/Main.java)

### Aliyun OSS
> make sure you have set OSS_ACCESS_KEY_ID and OSS_ACCESS_KEY_SECRET, before running code
1. download from aliyun OSS [Download Object](./aliyun-oss/src/main/java/org/example/Download.java)
2. upload from aliyun OSS [Upload Object](./aliyun-oss/src/main/java/org/example/Upload.java)