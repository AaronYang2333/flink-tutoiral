plugins {
    id("java")
}

group = "org.example"
version = "1.0.3"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.apache.arrow:arrow-vector:15.0.0")
    implementation("org.apache.arrow:arrow-memory:15.0.0")
    implementation("org.apache.arrow:flight-core:15.0.0")

    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
}

tasks.test {
    useJUnitPlatform()
}
