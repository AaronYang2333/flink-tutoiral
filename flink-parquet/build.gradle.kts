//import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    java
    id("com.github.johnrengelman.shadow") version "8.1.1"
    id("com.github.davidmc24.gradle.plugin.avro") version "1.9.1"
}

repositories {
    mavenCentral()
}

val lombokDependency = "org.projectlombok:lombok:1.18.22"
var flinkVersion = "1.17.1"
var log4jVersion = "2.17.1"
dependencies {
    implementation("org.apache.flink:flink-file-sink-common:$flinkVersion")
    implementation("org.apache.flink:flink-connector-files:$flinkVersion")
    implementation("org.apache.flink:flink-parquet:$flinkVersion")
    implementation("org.apache.flink:flink-streaming-java:$flinkVersion")
    implementation("org.apache.flink:flink-clients:$flinkVersion")
    implementation("org.apache.flink:flink-connector-datagen:$flinkVersion")
    implementation("org.apache.flink:flink-s3-fs-hadoop:$flinkVersion")
    implementation("org.apache.flink:flink-oss-fs-hadoop:$flinkVersion")
    implementation("org.apache.parquet:parquet-avro:1.13.1") {
        exclude("org.apache.hadoop", "hadoop-client")
        exclude("it.unimi.dsi", "fastutil")
    }
    // https://mvnrepository.com/artifact/org.apache.hadoop/hadoop-mapreduce-client-core
    implementation("org.apache.hadoop:hadoop-mapreduce-client-core:3.3.6")

    implementation("org.apache.flink:flink-avro:$flinkVersion")
    implementation("org.apache.avro:avro:1.11.0")

    annotationProcessor(lombokDependency)
    implementation("org.apache.logging.log4j:log4j-slf4j-impl:$log4jVersion")
    implementation("org.apache.logging.log4j:log4j-api:$log4jVersion")
    implementation("org.apache.logging.log4j:log4j-core:$log4jVersion")
    implementation(lombokDependency)

    testImplementation("org.junit.jupiter:junit-jupiter:5.9.3")
    testRuntimeOnly("org.junit.platform:junit-platform-launcher")
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(11))
    }
}

//avro {
//    isCreateSetters.set(true)
//    isCreateOptionalGetters.set(false)
//    isGettersReturnOptional.set(false)
//    isOptionalGettersForNullableFieldsOnly.set(false)
//    fieldVisibility.set("PUBLIC")
//    outputCharacterEncoding.set("UTF-8")
//    stringType.set("String")
//    templateDirectory.set(null as String?)
//    isEnableDecimalLogicalType.set(true)
//}

tasks.named<Test>("test") {
    useJUnitPlatform()
}
//
//tasks {
//    named<ShadowJar>("shadowJar") {
//        archiveBaseName.set("connector-shadow")
//        archiveVersion.set("1.0")
//        archiveClassifier.set("")
//        manifest {
//            attributes(mapOf("Main-Class" to "com.example.connector.Main"))
//        }
//    }
//}
