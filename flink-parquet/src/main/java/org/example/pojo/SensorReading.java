package org.example.pojo;

import java.io.Serializable;
import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class SensorReading implements Serializable {
  private String id;
  private long timestamp;
  private double temperature;
}
