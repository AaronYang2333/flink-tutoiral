package org.example.pojo;

import java.io.Serializable;
import lombok.*;
import org.apache.avro.Schema;
import org.apache.avro.specific.SpecificRecordBase;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@org.apache.avro.specific.AvroGenerated
public class SensorReadingHasImplement extends SpecificRecordBase
    implements org.apache.avro.specific.SpecificRecord, Serializable {
  private String id;
  private long timestamp;
  private double temperature;

  @Override
  public Schema getSchema() {
    return null;
  }

  @Override
  public Object get(int field) {
    return null;
  }

  @Override
  public void put(int field, Object value) {}
}
