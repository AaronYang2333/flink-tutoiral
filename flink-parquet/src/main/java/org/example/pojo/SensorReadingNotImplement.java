package org.example.pojo;

import java.io.Serializable;
import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class SensorReadingNotImplement implements Serializable {
  private String id;
  private long timestamp;
  private double temperature;
}
