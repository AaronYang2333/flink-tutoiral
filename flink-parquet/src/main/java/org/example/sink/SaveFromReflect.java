package org.example.sink;

import java.nio.file.Files;
import java.time.Duration;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.connector.source.SourceReaderContext;
import org.apache.flink.api.connector.source.util.ratelimit.RateLimiterStrategy;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.connector.datagen.source.DataGeneratorSource;
import org.apache.flink.connector.datagen.source.GeneratorFunction;
import org.apache.flink.connector.file.sink.FileSink;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.core.fs.Path;
import org.apache.flink.core.plugin.PluginUtils;
import org.apache.flink.formats.parquet.avro.AvroParquetWriters;
import org.apache.flink.runtime.state.hashmap.HashMapStateBackend;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.filesystem.OutputFileConfig;
import org.apache.flink.streaming.api.functions.sink.filesystem.bucketassigners.DateTimeBucketAssigner;
import org.apache.flink.streaming.api.functions.sink.filesystem.rollingpolicies.OnCheckpointRollingPolicy;
import org.example.pojo.SensorReadingNotImplement;

public class SaveFromReflect {

  public static void main(String[] args) throws Exception {
    Configuration pluginConfiguration = new Configuration();
//    pluginConfiguration.setString("s3.access-key", "admin");
//    pluginConfiguration.setString("s3.secret-key", "ZrwpsezF1Lt85dxl");
//    pluginConfiguration.setString("s3.endpoint", "http://localhost:9000");
    pluginConfiguration.setString("fs.oss.endpoint", "http://oss-cn-hangzhou-zjy-d01-a.ops.cloud.zhejianglab.com/");
    pluginConfiguration.setString("fs.oss.accessKeyId", "jeVxbuzDU9uenX6A");
    pluginConfiguration.setString("fs.oss.accessKeySecret", "9wGpFFAaCBaOZ29RqknuF2ZpwENaBi");
//    pluginConfiguration.setString("allowed-fallback-filesystems", "oss");
    pluginConfiguration.setBoolean("s3.path.style.access", Boolean.TRUE);
    FileSystem.initialize(
        pluginConfiguration, PluginUtils.createPluginManagerFromRootFolder(pluginConfiguration));
    StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
    env.enableCheckpointing(5000L, CheckpointingMode.EXACTLY_ONCE);
    env.getCheckpointConfig()
        .setExternalizedCheckpointCleanup(
            CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
    env.getCheckpointConfig().setMinPauseBetweenCheckpoints(10000);
    env.getCheckpointConfig().setCheckpointTimeout(60000);
    env.getCheckpointConfig().setMaxConcurrentCheckpoints(1);
    env.setParallelism(1);
    env.setStateBackend(new HashMapStateBackend());
    env.getCheckpointConfig().setCheckpointStorage("file:///./checkpoints");

    DataGeneratorSource<SensorReadingNotImplement> sensorReadingDataGeneratorSource =
        new DataGeneratorSource<>(
            new TempSensorReadingGeneratorFunction(),
            50,
            RateLimiterStrategy.perSecond(10),
            TypeInformation.of(SensorReadingNotImplement.class));

    DataStreamSource<SensorReadingNotImplement> sensorReadingDataStreamSource =
        env.fromSource(
            sensorReadingDataGeneratorSource,
            WatermarkStrategy.<SensorReadingNotImplement>forBoundedOutOfOrderness(
                    Duration.ofSeconds(10))
                .withTimestampAssigner(
                    (SerializableTimestampAssigner<SensorReadingNotImplement>)
                        (bean, time) -> Long.parseLong(bean.getId().split("_")[1]))
                .withIdleness(Duration.ofSeconds(10)),
            "sensor_reading_datagen");

    java.nio.file.Path tempDirectory = Files.createTempDirectory(".");
    System.out.printf("tempDirectory -> %s%n", tempDirectory.toString());

    FileSink<SensorReadingNotImplement> fileSink =
        FileSink.forBulkFormat(
                new Path("oss://csst-prod/ay-test/user-data/home/ay"),
                AvroParquetWriters.forReflectRecord(SensorReadingNotImplement.class))
            //                new Path(tempDirectory.toUri()),
            //                AvroParquetWriters.forReflectRecord(SensorReadingNotImplement.class))
            .withOutputFileConfig(OutputFileConfig.builder().build())
            .withBucketAssigner(new DateTimeBucketAssigner<>())
            .withRollingPolicy(OnCheckpointRollingPolicy.build())
            .build();

    sensorReadingDataStreamSource.sinkTo(fileSink);
    //        .print();

    // Work!
    env.execute();
  }

  private static class TempSensorReadingGeneratorFunction
      implements GeneratorFunction<Long, SensorReadingNotImplement> {

    public RandomDataGenerator generator;

    @Override
    public void open(SourceReaderContext readerContext) throws Exception {
      generator = new RandomDataGenerator();
    }

    @Override
    public SensorReadingNotImplement map(Long value) throws Exception {
      return SensorReadingNotImplement.builder()
          .id("sensor_" + generator.nextInt(0, 10))
          .timestamp(System.currentTimeMillis())
          .temperature(generator.nextUniform(50, 100))
          .build();
    }
  }
}
