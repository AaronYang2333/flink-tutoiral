package org.example.sink;

import java.nio.file.Files;
import java.time.Duration;
import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.generic.GenericRecordBuilder;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.connector.source.SourceReaderContext;
import org.apache.flink.api.connector.source.util.ratelimit.RateLimiterStrategy;
import org.apache.flink.connector.datagen.source.DataGeneratorSource;
import org.apache.flink.connector.datagen.source.GeneratorFunction;
import org.apache.flink.connector.file.sink.FileSink;
import org.apache.flink.core.fs.Path;
import org.apache.flink.formats.parquet.avro.AvroParquetWriters;
import org.apache.flink.runtime.state.hashmap.HashMapStateBackend;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.filesystem.OutputFileConfig;
import org.apache.flink.streaming.api.functions.sink.filesystem.bucketassigners.DateTimeBucketAssigner;
import org.apache.flink.streaming.api.functions.sink.filesystem.rollingpolicies.OnCheckpointRollingPolicy;
import org.example.pojo.SensorReading;

public class SaveFromGeneric {

  public static void main(String[] args) throws Exception {
    StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
    env.enableCheckpointing(5000L, CheckpointingMode.EXACTLY_ONCE);
    env.setParallelism(4);
    env.setStateBackend(new HashMapStateBackend());
    env.getCheckpointConfig().setCheckpointStorage("file:///./checkpoints");

    Schema schema =
        SchemaBuilder.record("SensorReading")
            .namespace("org.example.pojo")
            .fields()
            .requiredString("id")
            .requiredLong("timestamp")
            .requiredDouble("temperature")
            .endRecord();

    GenericRecordBuilder genericRecordBuilder = new GenericRecordBuilder(schema);
    DataGeneratorSource<SensorReading> sensorReadingDataGeneratorSource =
        new DataGeneratorSource<>(
            new SensorReadingGeneratorFunction(),
            100,
            RateLimiterStrategy.perSecond(10),
            TypeInformation.of(SensorReading.class));

    DataStreamSource<SensorReading> sensorReadingDataStreamSource =
        env.fromSource(
            sensorReadingDataGeneratorSource,
            WatermarkStrategy.<SensorReading>forBoundedOutOfOrderness(Duration.ofSeconds(10))
                .withTimestampAssigner(
                    (SerializableTimestampAssigner<SensorReading>)
                        (bean, time) -> Long.parseLong(bean.getId().split("_")[1]))
                .withIdleness(Duration.ofSeconds(10)),
            "sensor_reading_datagen");

    java.nio.file.Path tempDirectory = Files.createTempDirectory(".");
    System.out.printf("tempDirectory -> %s%n", tempDirectory.toString());

    FileSink<GenericRecord> fileSink =
        FileSink.forBulkFormat(
                new Path(tempDirectory.toUri()), AvroParquetWriters.forGenericRecord(schema))
            .withOutputFileConfig(OutputFileConfig.builder().build())
            .withBucketAssigner(new DateTimeBucketAssigner<>())
            .withRollingPolicy(OnCheckpointRollingPolicy.build())
            .build();

    sensorReadingDataStreamSource
        .map(
            (MapFunction<SensorReading, GenericRecord>)
                value -> {
                  GenericData.Record build = genericRecordBuilder.build();

                  return genericRecordBuilder
                      .set("id", value.getId())
                      .set("timestamp", value.getTimestamp())
                      .set("temperature", value.getTemperature())
                      .build();
                })
        .sinkTo(fileSink);
    //        .print();

    // Caused by: java.io.NotSerializableException: org.apache.avro.generic.GenericRecordBuilder
    env.execute();
  }

  private static class SensorReadingGeneratorFunction
      implements GeneratorFunction<Long, SensorReading> {

    public RandomDataGenerator generator;

    @Override
    public void open(SourceReaderContext readerContext) throws Exception {
      generator = new RandomDataGenerator();
    }

    @Override
    public SensorReading map(Long value) throws Exception {
      return SensorReading.builder()
          .id("sensor_" + generator.nextInt(0, 10))
          .timestamp(System.currentTimeMillis())
          .temperature(generator.nextUniform(50, 100))
          .build();
    }
  }
}
