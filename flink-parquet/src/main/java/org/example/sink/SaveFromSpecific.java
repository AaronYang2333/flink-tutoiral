package org.example.sink;

import java.nio.file.Files;
import java.time.Duration;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.serialization.SimpleStringEncoder;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.connector.source.SourceReaderContext;
import org.apache.flink.api.connector.source.util.ratelimit.RateLimiterStrategy;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.connector.datagen.source.DataGeneratorSource;
import org.apache.flink.connector.datagen.source.GeneratorFunction;
import org.apache.flink.connector.file.sink.FileSink;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.core.fs.Path;
import org.apache.flink.core.plugin.PluginUtils;
import org.apache.flink.formats.parquet.avro.AvroParquetWriters;
import org.apache.flink.runtime.state.hashmap.HashMapStateBackend;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.filesystem.OutputFileConfig;
import org.apache.flink.streaming.api.functions.sink.filesystem.StreamingFileSink;
import org.apache.flink.streaming.api.functions.sink.filesystem.bucketassigners.DateTimeBucketAssigner;
import org.apache.flink.streaming.api.functions.sink.filesystem.rollingpolicies.OnCheckpointRollingPolicy;
import org.example.pojo.SensorReadingGenerated;

public class SaveFromSpecific {

  public static void main(String[] args) throws Exception {
    Configuration pluginConfiguration = new Configuration();
//    pluginConfiguration.setString("s3.access-key", "admin");
//    pluginConfiguration.setString("s3.secret-key", "ZrwpsezF1Lt85dxl");
//    pluginConfiguration.setString("s3.endpoint", "http://localhost:9000");
    pluginConfiguration.setString("s3.access-key", "jeVxbuzDU9uenX6A");
    pluginConfiguration.setString("s3.secret-key", "9wGpFFAaCBaOZ29RqknuF2ZpwENaBi");
    pluginConfiguration.setString("s3.endpoint", "http://oss-cn-hangzhou-zjy-d01-a.ops.cloud.zhejianglab.com/");
    pluginConfiguration.setString("allowed-fallback-filesystems", "oss");
//    pluginConfiguration.setBoolean("s3.path.style.access", Boolean.TRUE);
    FileSystem.initialize(
        pluginConfiguration, PluginUtils.createPluginManagerFromRootFolder(pluginConfiguration));

    StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
    env.enableCheckpointing(5000L, CheckpointingMode.EXACTLY_ONCE);
    env.setParallelism(4);
    env.setStateBackend(new HashMapStateBackend());
    env.getCheckpointConfig().setCheckpointStorage("file:///./checkpoints");

    DataGeneratorSource<SensorReadingGenerated> sensorReadingDataGeneratorSource =
        new DataGeneratorSource<>(
            new TempSensorReadingGeneratorFunction(),
            10,
            RateLimiterStrategy.perSecond(10),
            TypeInformation.of(SensorReadingGenerated.class));

    DataStreamSource<SensorReadingGenerated> sensorReadingDataStreamSource =
        env.fromSource(
            sensorReadingDataGeneratorSource,
            WatermarkStrategy.<SensorReadingGenerated>forBoundedOutOfOrderness(
                    Duration.ofSeconds(10))
                .withTimestampAssigner(
                    (SerializableTimestampAssigner<SensorReadingGenerated>)
                        (bean, time) -> Long.parseLong(bean.getId().split("_")[1]))
                .withIdleness(Duration.ofSeconds(10)),
            "sensor_reading_datagen");

    java.nio.file.Path tempDirectory = Files.createTempDirectory(".");
    System.out.printf("tempDirectory -> %s%n", tempDirectory.toString());

    StreamingFileSink<String> streamingFileSink = StreamingFileSink.forRowFormat(
            new Path("oss://csst-prod/ay-test/user-data/home/ay"),
            new SimpleStringEncoder<String>("UTF-8")
    ).build();

    FileSink<SensorReadingGenerated> fileSink =
        FileSink.forBulkFormat(
                    new Path("s3://csst-prod/ay-test/user-data/home/ay"),
                AvroParquetWriters.forSpecificRecord(SensorReadingGenerated.class))
            .withOutputFileConfig(OutputFileConfig.builder().build())
            .withBucketAssigner(new DateTimeBucketAssigner<>())
            .withRollingPolicy(OnCheckpointRollingPolicy.build())
            .build();

    sensorReadingDataStreamSource.sinkTo(fileSink);

    // Work!
    env.execute();
  }

  private static class TempSensorReadingGeneratorFunction
      implements GeneratorFunction<Long, SensorReadingGenerated> {

    public RandomDataGenerator generator;

    @Override
    public void open(SourceReaderContext readerContext) throws Exception {
      generator = new RandomDataGenerator();
    }

    @Override
    public SensorReadingGenerated map(Long value) throws Exception {
      return new SensorReadingGenerated(
          "sensor_" + generator.nextInt(0, 10),
          System.currentTimeMillis(),
          generator.nextUniform(50, 100));
    }
  }
}
