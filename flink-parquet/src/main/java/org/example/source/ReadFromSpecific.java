package org.example.source;

import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.connector.file.src.FileSource;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.core.fs.Path;
import org.apache.flink.core.plugin.PluginUtils;
import org.apache.flink.formats.parquet.avro.AvroParquetReaders;
import org.apache.flink.runtime.state.hashmap.HashMapStateBackend;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.example.pojo.SensorReadingGenerated;

/**
 * @author AaronY
 * @version 1.0
 * @since 2024/2/8
 */
public class ReadFromSpecific {

  public static void main(String[] args) throws Exception {
    Configuration pluginConfiguration = new Configuration();
    pluginConfiguration.setString("s3.access-key", "conti");
    pluginConfiguration.setString("s3.secret-key", "Conti@1234");
    pluginConfiguration.setString("s3.endpoint", "http://api-minio-dev.lab.zjvis.net:32080");
    pluginConfiguration.setString("allowed-fallback-filesystems", "oss");
    pluginConfiguration.setBoolean("s3.path.style.access", Boolean.TRUE);
    FileSystem.initialize(
        pluginConfiguration, PluginUtils.createPluginManagerFromRootFolder(pluginConfiguration));
    StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
    env.enableCheckpointing(5000L, CheckpointingMode.EXACTLY_ONCE);
    env.getCheckpointConfig()
        .setExternalizedCheckpointCleanup(
            CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
    env.getCheckpointConfig().setMinPauseBetweenCheckpoints(10000);
    env.getCheckpointConfig().setCheckpointTimeout(60000);
    env.getCheckpointConfig().setMaxConcurrentCheckpoints(1);
    env.setParallelism(1);
    env.setStateBackend(new HashMapStateBackend());
    env.getCheckpointConfig().setCheckpointStorage("file:///./checkpoints");

    final FileSource<SensorReadingGenerated> source =
        FileSource.forRecordStreamFormat(
                AvroParquetReaders.forSpecificRecord(SensorReadingGenerated.class),
                new Path("s3://user-data/home/conti/2024-02-18--09"))
            .build();
    // only one type of parquet data file could save in that dir path
    env.fromSource(source, WatermarkStrategy.noWatermarks(), "file-source").print();

    env.execute();
  }
}
