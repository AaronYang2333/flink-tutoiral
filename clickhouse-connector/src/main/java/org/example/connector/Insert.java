package org.example.connector;

import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.connector.jdbc.JdbcConnectionOptions;
import org.apache.flink.connector.jdbc.JdbcSink;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Insert {
  private static final String CLICKHOUSE_URL = "jdbc:clickhouse://192.168.56.106:18123/default";
  private static final String CLICKHOUSE_JDBC_DRIVER = "com.clickhouse.jdbc.ClickHouseDriver";
  private static final String CLICKHOUSE_USER_NAME = "";
  private static final String CLICKHOUSE_PASSWD = "";

  public static void main(String[] args) {
    final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

    String sql = "insert into sensor_reading(id, timestamp, temperature) values(?, ?, ?)";
    Tuple3<String, Long, Double> tuple3 =
        Tuple3.of("sensor_122", 1706078482500L, 43.48859071739989);

    env.fromElements(tuple3)
        .returns(Types.TUPLE(Types.STRING, Types.LONG, Types.DOUBLE))
        .addSink(
            JdbcSink.sink(
                sql,
                (ps, tp) -> {
                  ps.setString(1, tp.f0);
                  ps.setLong(2, tp.f1);
                  ps.setDouble(3, tp.f2);
                },
                new JdbcConnectionOptions.JdbcConnectionOptionsBuilder()
                    .withUrl(CLICKHOUSE_URL)
                    .withDriverName(CLICKHOUSE_JDBC_DRIVER)
                    .withUsername(CLICKHOUSE_USER_NAME)
                    .withPassword(CLICKHOUSE_PASSWD)
                    .build()));

    try {
      env.execute();
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }
}
