package org.example.connector;

import java.util.List;
import org.apache.flink.connector.jdbc.JdbcConnectionOptions;
import org.apache.flink.connector.jdbc.JdbcSink;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Delete {
  private static final String CLICKHOUSE_URL = "jdbc:clickhouse://192.168.56.106:18123/default";
  private static final String CLICKHOUSE_JDBC_DRIVER = "com.clickhouse.jdbc.ClickHouseDriver";
  private static final String CLICKHOUSE_USER_NAME = "";
  private static final String CLICKHOUSE_PASSWD = "";

  public static void main(String[] args) {
    final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

    String sql = "alter table sensor_reading delete where id = ?";

    env.fromCollection(List.of("sensor_121", "sensor_122"))
        .addSink(
            JdbcSink.sink(
                sql,
                (ps, value) -> ps.setString(1, value),
                new JdbcConnectionOptions.JdbcConnectionOptionsBuilder()
                    .withUrl(CLICKHOUSE_URL)
                    .withDriverName(CLICKHOUSE_JDBC_DRIVER)
                    .withUsername(CLICKHOUSE_USER_NAME)
                    .withPassword(CLICKHOUSE_PASSWD)
                    .build()));

    try {
      env.execute();
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }
}
