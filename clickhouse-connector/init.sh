
podman run -d -P m.daocloud.io/docker.io/library/nginx

podman pull m.daocloud.io/docker.io/bitnami/clickhouse:23.12.3 \
&& podman run -d --replace -p 18123:8123 -p19000:9000 --name ay-clickhouse-server -e ALLOW_EMPTY_PASSWORD=yes --ulimit nofile=262144:262144 m.daocloud.io/docker.io/bitnami/clickhouse:23.12.3 \


## podman exec -it ay-clickhouse-server clickhouse-client

## create table
```sql
CREATE TABLE default.sensor_reading
(
  id String,
  timestamp Int64,
  temperature Float32
)
ENGINE = MergeTree()
ORDER BY id;
```