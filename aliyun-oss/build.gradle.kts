plugins {
    java
    id("com.github.johnrengelman.shadow") version "8.1.1"
}

repositories {
    mavenCentral()
}
group = "org.example"
version = "1.0.3"

repositories {
    mavenCentral()
}

dependencies {
    // https://mvnrepository.com/artifact/com.aliyun.oss/aliyun-sdk-oss
    implementation("com.aliyun.oss:aliyun-sdk-oss:3.17.4")

    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(11))
    }
}

tasks.named<Test>("test") {
    useJUnitPlatform()
}
