package org.example.connector.sink;

import java.nio.file.Files;
import java.time.Duration;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.serialization.SimpleStringEncoder;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.connector.source.util.ratelimit.RateLimiterStrategy;
import org.apache.flink.configuration.MemorySize;
import org.apache.flink.connector.datagen.source.DataGeneratorSource;
import org.apache.flink.connector.file.sink.FileSink;
import org.apache.flink.core.fs.Path;
import org.apache.flink.runtime.state.hashmap.HashMapStateBackend;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.filesystem.OutputFileConfig;
import org.apache.flink.streaming.api.functions.sink.filesystem.bucketassigners.DateTimeBucketAssigner;
import org.apache.flink.streaming.api.functions.sink.filesystem.rollingpolicies.DefaultRollingPolicy;
import org.example.connector.function.SensorReadingGeneratorFunction;
import org.example.connector.pojo.SensorReading;

public class SaveLocal {

  public static void main(String[] args) throws Exception {
    StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
    env.enableCheckpointing(5000L, CheckpointingMode.EXACTLY_ONCE);
    env.setParallelism(4);
    env.setStateBackend(new HashMapStateBackend());
    env.getCheckpointConfig().setCheckpointStorage("file:///./checkpoints");

    org.apache.flink.connector.datagen.source.DataGeneratorSource<SensorReading>
        sensorReadingDataGeneratorSource =
            new DataGeneratorSource<>(
                new SensorReadingGeneratorFunction(),
                10,
                RateLimiterStrategy.perSecond(10),
                TypeInformation.of(SensorReading.class));

    DataStreamSource<SensorReading> sensorReadingDataStreamSource =
        env.fromSource(
            sensorReadingDataGeneratorSource,
            WatermarkStrategy.<SensorReading>forBoundedOutOfOrderness(Duration.ofSeconds(10))
                .withTimestampAssigner(
                    (SerializableTimestampAssigner<SensorReading>)
                        (bean, time) -> Long.parseLong(bean.getId().split("_")[1]))
                .withIdleness(Duration.ofSeconds(10)),
            "sensor_reading_datagen");

    java.nio.file.Path tempDirectory = Files.createTempDirectory(".");
    System.out.printf("tempDirectory -> %s%n", tempDirectory.toString());

    FileSink<SensorReading> fileSink =
        FileSink.<SensorReading>forRowFormat(
                new Path(tempDirectory.toUri()), new SimpleStringEncoder<>("UTF-8"))
            .withOutputFileConfig(OutputFileConfig.builder().build())
            .withBucketAssigner(new DateTimeBucketAssigner<>())
            .withRollingPolicy(
                DefaultRollingPolicy.builder()
                    .withRolloverInterval(Duration.ofSeconds(20))
                    .withMaxPartSize(new MemorySize(1024))
                    .build())
            .build();

    sensorReadingDataStreamSource.sinkTo(fileSink);
    env.execute();
  }
}
