package org.example.connector.udf.fs.s3hadoop;

import java.net.URI;
import java.util.Collections;
import javax.annotation.Nullable;
import org.apache.flink.fs.s3.common.AbstractS3FileSystemFactory;
import org.apache.flink.fs.s3.common.writer.S3AccessHelper;
import org.apache.flink.fs.s3hadoop.HadoopS3AccessHelper;
import org.apache.flink.fs.s3hadoop.S3FileSystemFactory;
import org.apache.flink.fs.s3hadoop.common.HadoopConfigLoader;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.s3a.S3AFileSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author AaronY
 * @version 1.0
 * @since 2024/2/23
 */
public class S3UFileSystemFactory extends AbstractS3FileSystemFactory {
  private static final Logger LOG = LoggerFactory.getLogger(S3FileSystemFactory.class);

  private static final String[] FLINK_CONFIG_PREFIXES = {"s3.", "s3u.", "fs.s3u."};

  private static final String[][] MIRRORED_CONFIG_KEYS = {
    {"fs.s3u.access-key", "fs.s3u.access.key"},
    {"fs.s3u.secret-key", "fs.s3u.secret.key"},
    {"fs.s3u.path-style-access", "fs.s3u.path.style.access"}
  };

  public S3UFileSystemFactory() {
    super("Hadoop s3u file system", createHadoopConfigLoader());
  }

  @Override
  public String getScheme() {
    return "s3u";
  }

  static HadoopConfigLoader createHadoopConfigLoader() {
    return new HadoopConfigLoader(
        FLINK_CONFIG_PREFIXES,
        MIRRORED_CONFIG_KEYS,
        "fs.s3u.",
        Collections.emptySet(),
        Collections.emptySet(),
        "");
  }

  @Override
  protected org.apache.hadoop.fs.FileSystem createHadoopFileSystem() {
    return new S3AFileSystem();
  }

  @Override
  protected URI getInitURI(URI fsUri, org.apache.hadoop.conf.Configuration hadoopConfig) {
    final String scheme = fsUri.getScheme();
    final String authority = fsUri.getAuthority();

    if (scheme == null && authority == null) {
      fsUri = org.apache.hadoop.fs.FileSystem.getDefaultUri(hadoopConfig);
    } else if (scheme != null && authority == null) {
      URI defaultUri = org.apache.hadoop.fs.FileSystem.getDefaultUri(hadoopConfig);
      if (scheme.equals(defaultUri.getScheme()) && defaultUri.getAuthority() != null) {
        fsUri = defaultUri;
      }
    }

    LOG.debug("Using scheme {} for s3u file system backing the S3 File System", fsUri);

    return fsUri;
  }

  @Nullable
  @Override
  protected S3AccessHelper getS3AccessHelper(FileSystem fs) {
    final S3AFileSystem s3Afs = (S3AFileSystem) fs;
    return new HadoopS3AccessHelper(s3Afs, s3Afs.getConf());
  }
}
