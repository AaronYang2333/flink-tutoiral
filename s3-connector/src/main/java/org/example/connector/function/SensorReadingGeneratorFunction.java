package org.example.connector.function;

import org.apache.commons.math3.random.RandomDataGenerator;
import org.apache.flink.api.connector.source.SourceReaderContext;
import org.apache.flink.connector.datagen.source.GeneratorFunction;
import org.example.connector.pojo.SensorReading;

/**
 * @author AaronY
 * @version 1.0
 * @since 2024/2/2
 */
public class SensorReadingGeneratorFunction implements GeneratorFunction<Long, SensorReading> {

  public RandomDataGenerator generator;

  @Override
  public void open(SourceReaderContext readerContext) throws Exception {
    generator = new RandomDataGenerator();
  }

  @Override
  public SensorReading map(Long value) throws Exception {
    return SensorReading.builder()
        .id("sensor_" + generator.nextInt(0, 10))
        .timestamp(System.currentTimeMillis())
        .temperature(generator.nextUniform(50, 100))
        .build();
  }
}
