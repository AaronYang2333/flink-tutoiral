package org.example.connector.source;

import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.connector.file.src.FileSource;
import org.apache.flink.connector.file.src.reader.TextLineInputFormat;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.core.fs.Path;
import org.apache.flink.core.plugin.PluginUtils;
import org.apache.flink.runtime.state.hashmap.HashMapStateBackend;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * @author AaronY
 * @version 1.0
 * @since 2024/2/8
 */
public class ReadS3 {

  public static void main(String[] args) throws Exception {
    Configuration pluginConfiguration = new Configuration();
    pluginConfiguration.setString("s3a.access-key", "conti");
    pluginConfiguration.setString("s3a.secret-key", "Conti@1234");
    pluginConfiguration.setString("s3a.connection.maximum", "1000");
    pluginConfiguration.setString("s3a.endpoint", "http://api-minio-dev.lab.zjvis.net:32080");
    pluginConfiguration.setBoolean("s3a.path.style.access", Boolean.TRUE);
    FileSystem.initialize(
        pluginConfiguration, PluginUtils.createPluginManagerFromRootFolder(pluginConfiguration));

    StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
    env.enableCheckpointing(5000L, CheckpointingMode.EXACTLY_ONCE);
    env.setParallelism(1);
    env.setStateBackend(new HashMapStateBackend());
    env.getCheckpointConfig().setCheckpointStorage("file:///./checkpoints");

    final FileSource<String> source =
        FileSource.forRecordStreamFormat(
                new TextLineInputFormat(), new Path("s3a://user-data/home/conti/2024-02-08--10"))
            //                        .monitorContinuously(Duration.ofSeconds(1L))
            .build();
    env.fromSource(source, WatermarkStrategy.noWatermarks(), "file-source").print();

    env.execute();
  }
}
