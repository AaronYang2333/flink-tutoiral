package org.example.connector.source;

import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.connector.file.src.FileSource;
import org.apache.flink.connector.file.src.reader.TextLineInputFormat;
import org.apache.flink.core.fs.Path;
import org.apache.flink.runtime.state.hashmap.HashMapStateBackend;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * @author AaronY
 * @version 1.0
 * @since 2024/2/8
 */
public class ReadLocal {

  public static void main(String[] args) throws Exception {

    StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
    env.enableCheckpointing(5000L, CheckpointingMode.EXACTLY_ONCE);
    env.setParallelism(1);
    env.setStateBackend(new HashMapStateBackend());
    env.getCheckpointConfig().setCheckpointStorage("file:///./checkpoints");

    final FileSource<String> fileSource =
        FileSource.forRecordStreamFormat(
                new TextLineInputFormat(),
                new Path("file:///e:/Projects/flink-tutorial/resources/pojo"))
            //                        .monitorContinuously(Duration.ofSeconds(1L))
            .build();
    env.fromSource(fileSource, WatermarkStrategy.noWatermarks(), "file-source").print();

    env.execute();
  }
}
