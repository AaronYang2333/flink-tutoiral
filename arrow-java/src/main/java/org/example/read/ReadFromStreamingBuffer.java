package org.example.read;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.arrow.memory.BufferAllocator;
import org.apache.arrow.memory.RootAllocator;
import org.apache.arrow.vector.ipc.ArrowStreamReader;

/**
 * @author AaronY
 * @version 1.0
 * @since 2024/2/5
 */
public class ReadFromStreamingBuffer {

  public static void main(String[] args) {
    Path path = Paths.get("./streaming_to_file.arrow");
    try (BufferAllocator rootAllocator = new RootAllocator();
        ArrowStreamReader reader =
            new ArrowStreamReader(
                new ByteArrayInputStream(Files.readAllBytes(path)), rootAllocator)) {
      while (reader.loadNextBatch()) {
        System.out.print(reader.getVectorSchemaRoot().contentToTSVString());
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
