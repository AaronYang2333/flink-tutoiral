package org.example.write;

import static java.util.Arrays.asList;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.channels.Channels;
import org.apache.arrow.memory.BufferAllocator;
import org.apache.arrow.memory.RootAllocator;
import org.apache.arrow.vector.IntVector;
import org.apache.arrow.vector.VarCharVector;
import org.apache.arrow.vector.VectorSchemaRoot;
import org.apache.arrow.vector.ipc.ArrowStreamWriter;
import org.apache.arrow.vector.types.pojo.ArrowType;
import org.apache.arrow.vector.types.pojo.Field;
import org.apache.arrow.vector.types.pojo.FieldType;
import org.apache.arrow.vector.types.pojo.Schema;

/**
 * @author AaronY
 * @version 1.0
 * @since 2024/2/5
 */
public class WriteStreamingBuffer {

  public static void main(String[] args) {
    try (BufferAllocator rootAllocator = new RootAllocator()) {
      Field name = new Field("name", FieldType.nullable(new ArrowType.Utf8()), null);
      Field age = new Field("age", FieldType.nullable(new ArrowType.Int(32, true)), null);
      Schema schemaPerson = new Schema(asList(name, age));
      try (VectorSchemaRoot vectorSchemaRoot =
          VectorSchemaRoot.create(schemaPerson, rootAllocator)) {
        VarCharVector nameVector = (VarCharVector) vectorSchemaRoot.getVector("name");
        nameVector.allocateNew(3);
        nameVector.set(0, "David".getBytes());
        nameVector.set(1, "Gladis".getBytes());
        nameVector.set(2, "Juan".getBytes());
        IntVector ageVector = (IntVector) vectorSchemaRoot.getVector("age");
        ageVector.allocateNew(3);
        ageVector.set(0, 10);
        ageVector.set(1, 20);
        ageVector.set(2, 30);
        vectorSchemaRoot.setRowCount(3);
        try (ByteArrayOutputStream out = new ByteArrayOutputStream();
            ArrowStreamWriter writer =
                new ArrowStreamWriter(vectorSchemaRoot, null, Channels.newChannel(out))) {
          writer.start();
          writer.writeBatch();
          System.out.println("Number of rows written: " + vectorSchemaRoot.getRowCount());
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
