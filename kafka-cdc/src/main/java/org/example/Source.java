package org.example;

import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Source {
  public static void main(String[] args) throws Exception {

    StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

    env.enableCheckpointing(3000);

    String topic = "quickstart-events";

    KafkaSource<String> source =
        KafkaSource.<String>builder()
            .setBootstrapServers("192.168.56.106:9092")
            .setTopics(topic)
            .setGroupId("test-consumer-group")
            .setStartingOffsets(OffsetsInitializer.earliest())
            .setValueOnlyDeserializer(new SimpleStringSchema())
            .build();

    env.fromSource(source, WatermarkStrategy.noWatermarks(), "Kafka Source").print();

    env.execute("Connect Kafka");
  }
}
