package org.example;

import java.util.List;
import java.util.Properties;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.example.serialization.SimpleStringSerializationSchema;

/**
 * @author AaronY
 * @version 1.0
 * @since 2024/3/7
 */
public class Sink {

  public static void main(String[] args) throws Exception {

    StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

    env.enableCheckpointing(3000);

    String topic = "quickstart-events";

    Properties properties = new Properties();
    properties.setProperty("bootstrap.servers", "192.168.56.106:9092");
    properties.setProperty("group.id", "test-consumer-group");

    env.fromCollection(List.of("a", "b", "c", "d", "e", "f"))
        .setParallelism(1)
        .addSink(
            new FlinkKafkaProducer<>(
                topic,
                SimpleStringSerializationSchema.builder().topic(topic).build(),
                properties,
                FlinkKafkaProducer.Semantic.EXACTLY_ONCE))
        .setParallelism(1);

    env.execute("Connect Kafka");
  }
}
