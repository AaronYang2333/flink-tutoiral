package org.example.serialization;

import java.nio.charset.StandardCharsets;
import javax.annotation.Nullable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import org.apache.flink.streaming.connectors.kafka.KafkaSerializationSchema;
import org.apache.kafka.clients.producer.ProducerRecord;

/**
 * @author AaronY
 * @version 1.0
 * @since 2024/3/7
 */
@Builder
@AllArgsConstructor
public class SimpleStringSerializationSchema implements KafkaSerializationSchema<String> {

  private String topic;

  @Override
  public ProducerRecord<byte[], byte[]> serialize(String element, @Nullable Long timestamp) {
    return new ProducerRecord<>(topic, element.getBytes(StandardCharsets.UTF_8));
  }
}
